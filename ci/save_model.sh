#!/usr/bin/env bash
python3 -m pip install awscli
apt update && apt install jq --no-install-recommends -y

git_tag=$1
if [ -z "$git_tag" ]; then
  echo "Error when trying to retrieve the git tag to save the experiment. Cannot continue."
  exit 1
fi

image_name=$(bash ci/get_config_value.sh image_name)
if [ -z "$image_name" ]; then
  echo "The config 'image_name' in .sagify.json must be set. Cannot continue."
  exit 1
fi

s3_model_source_path=$(jq .ModelArtifacts.S3ModelArtifacts job_result.json)
if [ -z "$s3_model_source_path" ]; then
  echo "The value of 's3_model_source_path' could not be extracted from job_result.json file. Cannot continue."
  exit 1
fi
s3_model_source_path_cleaned=$(echo "$s3_model_source_path" | tr -d '"')

s3_model_destination_path="s3://d-ew1-ted-ai-ml-models/models/$image_name/$git_tag/model.tar.gz"

echo "Copying data from '$s3_model_source_path_cleaned' to '$s3_model_destination_path'"
aws s3 cp "$s3_model_source_path_cleaned" "$s3_model_destination_path"
